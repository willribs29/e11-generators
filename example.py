# from sys import getsizeof
#
# def gen_1():
#     index = 0
#     while True:
#         yield index
#         index +=1
# my_gen = gen_1()
# print(next(my_gen))
# print(next(my_gen))
# print(next(my_gen))
# print(next(my_gen))

def ascii_gen(start: int, quantity: int):
    index = start
    while index < start + quantity:
        yield chr(index)
        index += 1

my_gen = ascii_gen(65,10)
print(ascii_gen)
for letter in my_gen:
    print(letter)

print
(next(my_gen))
